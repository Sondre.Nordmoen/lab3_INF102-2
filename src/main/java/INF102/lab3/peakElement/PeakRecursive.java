package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        // Ensure empty and short lists are caught
        if (numbers.isEmpty()) {
            throw new RuntimeException("List cannot be empty");
        } else if (numbers.size() == 1) {
            return numbers.get(0);
        }
        // Go from the first element.
        // Going in reverse might be quicker, as the list
        // does not need to be shifted each time we remove an element
        // Using 0 and 1 does look cleaner however, as can be seen below
        if (numbers.get(0) >= numbers.get(1)) {
            return numbers.get(0);
        }
        numbers.remove(0);
        return peakElement(numbers);
    }

    public int peakElementImproved(List<Integer> numbers) {
        if (numbers.isEmpty()) {
            throw new RuntimeException("List cannot be empty");
        } else if (numbers.size() == 1) {
            return numbers.get(0);
        }
        int size = numbers.size();
        if (numbers.get(size-1) >= (numbers.get(size-2))) {
            return numbers.get(size-1);
        }
        numbers.remove(size-1);
        return peakElementImproved(numbers);
    }

}
