package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        long retVal;
        // Can avoid 1 loop with if (list.size() == 1) ? return 0:;
        // but list.isEmpty() is cleaner
        if (list.isEmpty()) {
            return 0;
        } else {
            // Removing from the tail of the list
            // to avoid shifting list by 1
            retVal = list.get(list.size() - 1);
            list.remove(list.size() - 1);
            return retVal + sum(list);
        }
    }
    

}
