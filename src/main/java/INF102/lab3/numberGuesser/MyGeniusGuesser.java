package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        // It is some variation of binary search
        // attempting to circle in on the correct number
        int currentGuess = 500;
        int someLowerBound = 0;
        int someUpperBound;
        int result;
        while (true) {
            // Find some estimate of lower and upper
            // bounds. This makes the algorithm easier
            // too keep within a reasonable number of steps
            result = number.guess(currentGuess);
            if (result == 1) {
                someUpperBound = currentGuess;
                break;
            } else if (result == -1) {
                someLowerBound = currentGuess;
            } else {
                return currentGuess;
            }
            currentGuess *= 2;
        }
        currentGuess = someUpperBound - (someUpperBound - someLowerBound) / 2;
        result = number.guess(currentGuess);
        int previousGuess;
        int tempGuess;
        if (result == 1) {
            previousGuess = someLowerBound;
        } else  {
            previousGuess = someUpperBound;
        }

        while (result != 0) {
            if (result == 1) {
                // Too large
                // Half the distance between current and previous
                tempGuess = currentGuess;
                currentGuess = currentGuess - Math.abs(currentGuess - previousGuess) / 2;
                previousGuess = tempGuess;
                if (currentGuess == tempGuess) {
                    // If they get stuck in an infinite loop due to
                    // integer division
                    currentGuess--;
                }
            } else if (result == -1) {
                // Too small
                tempGuess = currentGuess;
                currentGuess = currentGuess + Math.abs(currentGuess - previousGuess) / 2;
                previousGuess = tempGuess;
                if (currentGuess == tempGuess) {
                    currentGuess++;
                }
            }
            result = number.guess(currentGuess);
        }

        return currentGuess;
    }

}
